using System.Collections.Generic;

namespace FlockingBackend
{
    public delegate void CalculateMoveVector(List<Sparrow> sparrows);

    public delegate void MoveBird();

    public delegate void CalculateRavenAvoidance(Raven raven);
}