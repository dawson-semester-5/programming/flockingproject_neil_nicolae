using System;
using System.Collections.Generic;

namespace FlockingBackend
{
    ///<summary>
    ///This class is used to represent a single sparrow. 
    ///This class is just a starting point. Complete the TODO sections
    ///</summary>
    public class Sparrow : Bird
    {
        public Sparrow()
        {
        }

        public Sparrow(float PosX, float PosY, float VelX, float VelY)
            : base(PosX, PosY, VelX, VelY)
        {
        }

        ///<summary>
        ///This method is an event handler to calculate and set amountToSteer vector using the flocking algorithm rules
        ///</summary>
        ///<param name="sparrows">List of sparrows</param>
        public override void CalculateBehaviour(List<Sparrow> sparrows)
        {
            amountToSteer = Cohesion(sparrows) + Avoidance(sparrows) + Alignment(sparrows);
        }

        /// <summary>
        /// Calculates the correct alignment vector for this sparrow
        /// </summary>
        /// <param name="sparrows">list of sparrows around this one</param>
        /// <returns>new vector for alignment</returns>
        public Vector2 Alignment(List<Sparrow> sparrows)
        {
            int sparrowsInNeighborRadius = 0;
            Vector2 alignmentSum = new Vector2(0, 0);

            // calculate average velocity of sparrows in neighbor radius
            foreach (Sparrow sparrow in sparrows)
            {
                if (Vector2.DistanceSquared(this.Velocity, sparrow.Velocity) <= World.NeighbourRadius)
                {
                    alignmentSum += sparrow.Velocity;
                    sparrowsInNeighborRadius++;
                }
            }

            if (sparrowsInNeighborRadius == 0)
            {
                return new Vector2(0, 0);
            }

            Vector2 averageAlignment = alignmentSum / sparrowsInNeighborRadius;
            Vector2 normalizedAverageAlignment = Vector2.Normalize(averageAlignment);
            Vector2 normalizedAverageAlignmentMaxSpeed = normalizedAverageAlignment * World.MaxSpeed;

            // return normalized average velocity vector with correct world speed minus own velocity
            return Vector2.Normalize(normalizedAverageAlignmentMaxSpeed - this.Velocity);
        }

        /// <summary>
        /// Calculates the correct cohesion vector for this sparrow
        /// </summary>
        /// <param name="sparrows">list of sparrows around this one</param>
        /// <returns>new vector for cohesion</returns>
        public Vector2 Cohesion(List<Sparrow> sparrows)
        {
            int sparrowsInNeighborRadius = 0;
            Vector2 cohesionSum = new Vector2(0, 0);

            // calculate average position of sparrows in neighbor radius
            foreach (Sparrow sparrow in sparrows)
            {
                if (Vector2.DistanceSquared(this.Position, sparrow.Position) < Math.Pow(World.NeighbourRadius, 2))
                {
                    cohesionSum += sparrow.Position;
                    sparrowsInNeighborRadius++;
                }
            }

            if (sparrowsInNeighborRadius == 0)
            {
                return new Vector2(0, 0);
            }

            Vector2 averagePosition = cohesionSum / sparrowsInNeighborRadius;
            Vector2 averageCohesionMinusPositionNormalized = Vector2.Normalize(averagePosition - this.Position);

            // return normalized average position vector with correct speed minus own position
            return Vector2.Normalize(averageCohesionMinusPositionNormalized * World.MaxSpeed - this.Velocity);
        }

        /// <summary>
        /// Calculates the correct avoidance vector for this sparrow
        /// </summary>
        /// <param name="sparrows">list of sparrows around this one</param>
        /// <returns>new vector for avoidance</returns>
        public Vector2 Avoidance(List<Sparrow> sparrows)
        {
            Vector2 avoidanceSum = new Vector2(0, 0);
            int sparrowsInAvoidanceRadius = 0;

            foreach (Sparrow sparrow in sparrows)
            {
                float distance = Vector2.DistanceSquared(this.Position, sparrow.Position);
                if (distance != 0 && distance < Math.Pow(World.AvoidanceRadius, 2) && this != sparrow)
                {
                    Vector2 distanceVector = this.Position - sparrow.Position;

                    distanceVector /= distance;
                    avoidanceSum += distanceVector;
                    sparrowsInAvoidanceRadius++;
                }
            }

            if (sparrowsInAvoidanceRadius == 0)
            {
                return new Vector2(0, 0);
            }

            Vector2 averageAvoidance = avoidanceSum / sparrowsInAvoidanceRadius;
            Vector2 normalizedAverageAvoidance = Vector2.Normalize(averageAvoidance);
            Vector2 normalizedAverageAvoidanceMaxSpeed = normalizedAverageAvoidance * World.MaxSpeed;

            // return normalized average velocity vector with correct speed
            return Vector2.Normalize(normalizedAverageAvoidanceMaxSpeed - this.Velocity);
        }

        ///<summary>
        ///This method is an event handler to calculate and update amountToSteer vector with the amount to steer to flee a chasing raven
        ///</summary>
        ///<param name="raven">A Raven object</param>
        public void CalculateRavenAvoidance(Raven raven)
        {
            amountToSteer += FleeRaven(raven);
        }

        /// <summary>
        /// Calculates the correct flee vector for this sparrow
        /// </summary>
        /// <param name="raven">raven to flee</param>
        /// <returns>new vector for fleeing</returns>

        private Vector2 FleeRaven(Raven raven)
        {

            float distance = Vector2.DistanceSquared(this.Position, raven.Position);

            if (distance < Math.Pow(World.AvoidanceRadius, 2))
            {
                Vector2 distanceVector = this.Position - raven.Position;
                distanceVector /= distance;

                // normalize distance vector and multiply with world max speed
                return Vector2.Normalize(distanceVector) * World.MaxSpeed;
            }

            return new Vector2(0, 0);
        }
    }
}