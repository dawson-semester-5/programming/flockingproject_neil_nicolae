using System;
using System.Collections.Generic;

namespace FlockingBackend
{
    ///<summary>
    ///This class is used to represent a single raven. 
    ///This class is just a starting point. Complete the TODO sections
    ///</summary>
    public class Raven : Bird
    {
        public Raven()
        {
        }

        public Raven(float PosX, float PosY, float VelX, float VelY)
            : base(PosX, PosY, VelX, VelY)
        {
        }

        ///<summary>
        ///This method is an event handler to calculate and set amountToSteer vector
        ///</summary>
        ///<param name="sparrows">List of sparrows</param>
        public override void CalculateBehaviour(List<Sparrow> sparrows)
        {
            amountToSteer = ChaseSparrow(sparrows);
        }

        ///<summary>
        /// This method is used to calculate the amountToSteer vector to chase a sparrow
        /// </summary>
        /// <param name="sparrows">List of sparrows</param>
        /// <returns>Vector to chase a sparrow</returns>
        public Vector2 ChaseSparrow(List<Sparrow> sparrows)
        {
            Sparrow nearestSparrow = null;
            float nearestSparrowDistance = (float)Math.Pow(World.AvoidanceRadius, 2);
            
            // find nearest sparrow to this Raven
            foreach (Sparrow sparrow in sparrows)
            {
                float distance = Vector2.DistanceSquared(sparrow.Position, this.Position);

                if (distance < nearestSparrowDistance)
                {
                    nearestSparrow = sparrow;
                    nearestSparrowDistance = distance;
                }
            }
            if (nearestSparrow == null) return new Vector2(0, 0);
            return nearestSparrow.Position - Position;
        }

        public override void Move()
        {
            Velocity += amountToSteer;
            Velocity = Vector2.Normalize(Velocity) * World.MaxSpeed;
            Position += Velocity;
            AppearOnOppositeSide();
        }
    }
}
