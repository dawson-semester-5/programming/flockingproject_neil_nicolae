using System;

namespace FlockingBackend
{
    public struct Vector2
    {
        public float Vx { get; }
        public float Vy { get; }

        public Vector2(float x, float y)
        {
            this.Vx = x;
            this.Vy = y;
        }

        public static Vector2 operator +(Vector2 v1, Vector2 v2)
        {
            return new Vector2(v1.Vx + v2.Vx, v1.Vy + v2.Vy);
        }

        public static Vector2 operator -(Vector2 v1, Vector2 v2)
        {
            return new Vector2(v1.Vx - v2.Vx, v1.Vy - v2.Vy);
        }

        public static Vector2 operator /(Vector2 v, float div)
        {
            if (div == 0) return v;
            return new Vector2(v.Vx / div, v.Vy / div);
        }

        public static Vector2 operator *(Vector2 v, float scalar)
        {
            return new Vector2(v.Vx * scalar, v.Vy * scalar);
        }

        public static float DistanceSquared(Vector2 v1, Vector2 v2)
        {
            if (float.IsNaN(v1.Vx) || float.IsNaN(v1.Vy) || float.IsNaN(v2.Vx) || float.IsNaN(v2.Vy))
            {
                return 0;
            }
            return (float)(v1.Vx - v2.Vx) * (v1.Vx - v2.Vx) + (v1.Vy - v2.Vy) * (v1.Vy - v2.Vy);
        }

        public static Vector2 Normalize(Vector2 v)
        {
            float magnitude = (float)Math.Sqrt(v.Vx * v.Vx + v.Vy * v.Vy);
            if (magnitude == 0) return v;
            return new Vector2(v.Vx / magnitude, v.Vy / magnitude);
        }
    }
}