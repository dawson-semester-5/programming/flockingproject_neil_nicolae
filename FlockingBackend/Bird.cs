using System;
using System.Collections.Generic;

namespace FlockingBackend
{
    public abstract class Bird
    {
        /// <summary>
        /// rotation of the bird
        /// </summary>
        /// <value> Property <c>Rotation</c> to rotate the Sparrow to face the direction it is moving toward.</value>
        public float Rotation
        {
            get => (float)Math.Atan2(this.Velocity.Vy, this.Velocity.Vx);
        }

        /// <summary>
        /// Position of the bird on screen
        /// </summary>
        /// <value>Vector2 representing position relative to the world's dimensions</value>
        public Vector2 Position { get; protected set; }

        /// <summary>
        /// Current speed of the bird and its angle represented in a vector
        /// </summary>
        /// <value>Vector2 with a max value of the world's max speed</value>
        public Vector2 Velocity { get; protected set; }

        protected Vector2 amountToSteer;

        /// <summary>
        /// Creates a bird with a random position and velocity
        /// </summary>
        public Bird()
        {
            Random rand = new Random();

            Position = new Vector2(rand.Next(0, World.Width), rand.Next(0, World.Height));
            Velocity = new Vector2(rand.Next(-World.MaxSpeed, World.MaxSpeed), rand.Next(-World.MaxSpeed, World.MaxSpeed));
            amountToSteer = new Vector2(0, 0);
        }

        /// <summary>
        /// Used to avoid random numbers when testing the constructor
        /// </summary>
        /// <param name="PosX">Position Vector X</param>
        /// <param name="PosY">Position Vector Y</param>
        /// <param name="VelX">Velocity Vector X</param>
        /// <param name="VelY">Velocity Vector Y</param>
        public Bird(float PosX, float PosY, float VelX, float VelY)
        {
            Position = new Vector2(PosX, PosY);
            Velocity = new Vector2(VelX, VelY);
            amountToSteer = new Vector2(0, 0);
        }

        /// <summary>
        /// protected helper method to make sparrows reappear on the opposite edge if they go outside the bounds of the screen
        /// </summary>
        protected void AppearOnOppositeSide()
        {
            // Adjust position horizontally
            if (this.Position.Vx > World.Width)
            {
                this.Position = new Vector2(0, this.Position.Vy);
            }
            else if (this.Position.Vx < 0)
            {
                this.Position = new Vector2(World.Width, this.Position.Vy);
            }

            // Adjust position vertically
            if (this.Position.Vy > World.Height)
            {
                this.Position = new Vector2(this.Position.Vx, 0);
            }
            else if (this.Position.Vy < 0)
            {
                this.Position = new Vector2(this.Position.Vx, World.Height);
            }
        }

        /// <summary>
        /// This method is an event handler that updates the velocity and position of a bird
        /// Calculates the appropriate behaviour for a bird and sets the amountToSteer vector for a bird
        /// </summary>
        public abstract void CalculateBehaviour(List<Sparrow> sparrows);

        public virtual void Move()
        {
            Velocity += amountToSteer;
            Position += Velocity;
            AppearOnOppositeSide();
        }
    }
}