using System.Collections.Generic;

namespace FlockingBackend
{
    public class World
    {
        public static int InitialCount { get; }
        public static int Width { get; }
        public static int Height { get; }
        public static int MaxSpeed { get; }
        public static int NeighbourRadius { get; }
        public static int AvoidanceRadius { get; }

        static World()
        {
            InitialCount = 150;
            Width = 1000;
            Height = 500;
            MaxSpeed = 4;
            NeighbourRadius = 100;
            AvoidanceRadius = 50;
        }

        public Flock Flock { get; }
        public List<Sparrow> Sparrows { get; }
        public Raven Raven { get; }

        public World()
        {
            Flock = new Flock();

            Sparrows = new List<Sparrow>();
            for (int i = 0; i < InitialCount; i++)
            {
                Sparrows.Add(new Sparrow());
                // subscribe each of the sparrows to the flock
                Flock.Subscribe(
                    Sparrows[i].CalculateBehaviour,
                    Sparrows[i].Move,
                    Sparrows[i].CalculateRavenAvoidance
                );
            }

            Raven = new Raven();
            Flock.Subscribe(
                (Sparrows) => Raven.CalculateBehaviour(Sparrows),
                () => Raven.Move()
            );
        }

        public void Update()
        {
            Flock.RaiseMoveEvents(Sparrows, Raven);
        }
    }
}