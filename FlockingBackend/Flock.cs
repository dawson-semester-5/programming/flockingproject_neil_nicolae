using System;
using System.Collections.Generic;

namespace FlockingBackend
{
    ///<summary>
    ///This class is the subscriber class that each bird subscribes to. The class also raises the events to calculate movement vector and move the birds.
    ///This class is just a starting point. Complete the TODO sections
    ///</summary>
    public class Flock
    {
        public event CalculateMoveVector CalcMovementEvent;
        public event CalculateRavenAvoidance CalcRavenFleeEvent;
        public event MoveBird MoveEvent;

        ///<summary>
        ///This method subscribes to the events
        ///</summary>
        /// <param name="calcMovementEvent">The event that calculates the birds' steering movement vector</param>
        /// <param name="calcRavenFleeEvent">The event that calculates the movement vector to steer away from ravens</param>
        /// <param name="moveEvent">The event that moves the bird</param>
        public void Subscribe(CalculateMoveVector calcMovement, MoveBird moveBird, CalculateRavenAvoidance calcRavenFlee = null)
        {
            CalcMovementEvent += calcMovement;
            MoveEvent += moveBird;
            if (calcRavenFlee != null) CalcRavenFleeEvent += calcRavenFlee;
        }

        ///<summary>
        ///This method raises the calculate and move events
        ///</summary>
        ///<param name="sparrows">List of Sparrow objects</param>
        ///<param name="raven">A Raven object</param>
        public void RaiseMoveEvents(List<Sparrow> sparrows, Raven raven)
        {
            CalcMovementEvent.Invoke(sparrows);
            CalcRavenFleeEvent?.Invoke(raven);
            MoveEvent.Invoke();
        }
    }
}