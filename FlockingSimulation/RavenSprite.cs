using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using FlockingBackend;
using System;
using System.Collections.Generic;

namespace FlockingSimulation
{
    class RavenSprite : DrawableGameComponent
    {
        private Game1 game;
        private Raven raven;
        private World world;
        private Texture2D ravenTexture;
        private SpriteBatch _spriteBatch;
        // private GraphicsDeviceManager _graphics;

        public RavenSprite(Game1 game, World world) : base(game)
        {
            this.game = game;
            this.world = world;
            raven = world.Raven;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            ravenTexture = game.Content.Load<Texture2D>("raven");
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin();
            _spriteBatch.Draw(
                ravenTexture,
                new Microsoft.Xna.Framework.Vector2(raven.Position.Vx, raven.Position.Vy),
                null,
                Color.White,
                raven.Rotation,
                // Origin for rotation. 15,15 is center of texture. Will use constants to hold this value.
                new Microsoft.Xna.Framework.Vector2(15, 15),
                1,
                SpriteEffects.None,
                0
            );
            _spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}