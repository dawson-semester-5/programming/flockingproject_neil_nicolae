using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using FlockingBackend;
using System;
using System.Collections.Generic;

namespace FlockingSimulation
{
    class SparrowFlockSprite : DrawableGameComponent
    {
        private Game1 game;
        private Texture2D sparrowTexture;
        private SpriteBatch _spriteBatch;

        private World world;

        public SparrowFlockSprite(Game1 game, World world) : base(game)
        {
            this.game = game;
            this.world = world;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            sparrowTexture = game.Content.Load<Texture2D>("sparrow");
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin();
            foreach (Sparrow s in world.Sparrows)
            {
                _spriteBatch.Draw(
                    sparrowTexture,
                    new Microsoft.Xna.Framework.Vector2(s.Position.Vx, s.Position.Vy),
                    null,
                    Color.White,
                    s.Rotation,
                    // Origin for rotation. 10,10 is center of texture. Will use constants to hold this value.
                    new Microsoft.Xna.Framework.Vector2(10, 10),
                    1,
                    SpriteEffects.None,
                    0
                );
            }
            _spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}