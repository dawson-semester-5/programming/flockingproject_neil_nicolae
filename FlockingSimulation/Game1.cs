﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using FlockingBackend;
using System;
using System.Collections.Generic;

namespace FlockingSimulation
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private World world;
        private SparrowFlockSprite sparrowFlockSprite;
        private RavenSprite ravenSprite;

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            world = new World();
        }

        protected override void Initialize()
        {
            _graphics.PreferredBackBufferWidth = World.Width;
            _graphics.PreferredBackBufferHeight = World.Height;
            _graphics.ApplyChanges();

            // Initialize bird sprites
            sparrowFlockSprite = new SparrowFlockSprite(this, world);
            ravenSprite = new RavenSprite(this, world);

            this.Components.Add(sparrowFlockSprite);
            this.Components.Add(ravenSprite);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            world.Update();
            base.Update(gameTime);
        }

        private Color background = new Color(26, 28, 46);
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(background);
            // GraphicsDevice.Clear(Color.Hex("#1a1c2e"));
            base.Draw(gameTime);
        }
    }
}
