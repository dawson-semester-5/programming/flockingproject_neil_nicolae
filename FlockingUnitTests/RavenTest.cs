using Microsoft.VisualStudio.TestTools.UnitTesting;
using FlockingBackend;
using System.Collections.Generic;
using System;
using System.Reflection;


namespace FlockingUnitTests
{
    [TestClass]
    public class RavenTest
    {
        static Flock Flock;
        static Raven Raven;
        static List<Sparrow> Sparrows;

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            Flock = new Flock();
            Raven = new Raven();
            Sparrows = new List<Sparrow>();
        }

        [DataTestMethod]
        [DataRow(12, 0,
                123, 300, 0, 2,
                115, 290, 1, 1)]
        [DataRow(-1, 6,
                411, 233, 3, 1,
                400, 240, -2, 2)]
        [DataRow(-3, 3,
                12, 500, -4, 3,
                19, 487, 4, 1)]
        public void ChaseSparrow(
            float expectedVx, float expectedVy,
            float SparrowPosX, float SparrowPosY, float SparrowVelX, float SparrowVelY,
            float RavenPosX, float RavenPosY, float RavenVelX, float RavenVelY
            )
        {
            Sparrows.Add(new Sparrow(SparrowPosX, SparrowPosY, SparrowVelX, SparrowVelY));
            Sparrows.Add(new Sparrow(SparrowPosX + 1, SparrowPosY - 1, SparrowVelX - 2, SparrowVelY + 3));
            Sparrows.Add(new Sparrow(SparrowPosX - 12, SparrowPosY + 13, SparrowVelX + 1, SparrowVelY - 1));
            Sparrows.Add(new Sparrow(SparrowPosX + 16, SparrowPosY - 1, SparrowVelX - 2, SparrowVelY + 2));
            Sparrows.Add(new Sparrow(SparrowPosX + 4, SparrowPosY - 10, SparrowVelX + 1, SparrowVelY - 4));
            Sparrows.Add(new Sparrow(SparrowPosX - 2, SparrowPosY + 3, SparrowVelX + 1, SparrowVelY + 1));

            Raven = new Raven(RavenPosX, RavenPosY, RavenVelX, RavenVelY);
            Assert.AreEqual(new Vector2(expectedVx, expectedVy), Raven.ChaseSparrow(Sparrows));
        }
    }
}