using Microsoft.VisualStudio.TestTools.UnitTesting;
using FlockingBackend;
using System;

namespace FlockingUnitTests
{
    [TestClass]
    public class Vector2Test
    {
        [DataTestMethod]
        [DataRow(5, 2, 4, 3)]
        [DataRow(8.2f, -3.7f, 2, -4)]
        [DataRow(-9.2f, 2, -8, 1)]
        public void add(float x1, float y1, float x2, float y2)
        {
            Vector2 v1 = new Vector2(x1, y1);
            Vector2 v2 = new Vector2(x2, y2);

            Vector2 sumV = v1 + v2;

            Assert.AreEqual(x1 + x2, sumV.Vx);
            Assert.AreEqual(y1 + y2, sumV.Vy);

            // same as Microsoft Vector2
            var mv1 = new Microsoft.Xna.Framework.Vector2(x1, y1);
            var mv2 = new Microsoft.Xna.Framework.Vector2(x2, y2);
            Microsoft.Xna.Framework.Vector2 sumMV = mv1 + mv2;
            Assert.AreEqual(sumV.Vx, sumMV.X);
            Assert.AreEqual(sumV.Vy, sumMV.Y);
        }

        [DataTestMethod]
        [DataRow(5, 2, 4, 3)]
        [DataRow(8.2f, -3.7f, 2, -4)]
        [DataRow(-9.2f, 2, -8, 1)]
        public void subtract(float x1, float y1, float x2, float y2)
        {
            Vector2 v1 = new Vector2(x1, y1);
            Vector2 v2 = new Vector2(x2, y2);

            Vector2 differenceV = v1 - v2;

            Assert.AreEqual(x1 - x2, differenceV.Vx);
            Assert.AreEqual(y1 - y2, differenceV.Vy);


            // same as Microsoft Vector2
            var mv1 = new Microsoft.Xna.Framework.Vector2(x1, y1);
            var mv2 = new Microsoft.Xna.Framework.Vector2(x2, y2);
            Microsoft.Xna.Framework.Vector2 differenceMV = mv1 - mv2;
            Assert.AreEqual(differenceV.Vx, differenceMV.X);
            Assert.AreEqual(differenceV.Vy, differenceMV.Y);
        }

        [DataTestMethod]
        [DataRow(5, 2, 4)]
        [DataRow(8.2f, -3.7f, 2)]
        [DataRow(-9.2f, 2, -8)]
        public void divide(float x, float y, float divideBy)
        {
            Vector2 v = new Vector2(x, y);

            Vector2 quotientV = v / divideBy;

            Assert.AreEqual(x / divideBy, quotientV.Vx);
            Assert.AreEqual(y / divideBy, quotientV.Vy);

            // same as Microsoft Vector2
            var mv1 = new Microsoft.Xna.Framework.Vector2(x, y);
            Microsoft.Xna.Framework.Vector2 differenceMV = mv1 / divideBy;
            Assert.AreEqual(quotientV.Vx, differenceMV.X);
            Assert.AreEqual(quotientV.Vy, differenceMV.Y);
        }

        [DataTestMethod]
        [DataRow(5, 2, 4)]
        [DataRow(8.2f, -3.7f, 2)]
        [DataRow(-9.2f, 2, -8)]
        public void scale(float x, float y, float scalar)
        {
            Vector2 v = new Vector2(x, y);

            Vector2 scaledV = v * scalar;

            Assert.AreEqual(x * scalar, scaledV.Vx);
            Assert.AreEqual(y * scalar, scaledV.Vy);

            // same as Microsoft Vector2
            var mv1 = new Microsoft.Xna.Framework.Vector2(x, y);
            Microsoft.Xna.Framework.Vector2 differenceMV = mv1 * scalar;
            Assert.AreEqual(scaledV.Vx, differenceMV.X);
            Assert.AreEqual(scaledV.Vy, differenceMV.Y);
        }

        [DataTestMethod]
        [DataRow(5, 2, 0.9285f, 0.3714f)]
        [DataRow(6.6f, 9, 0.5914f, 0.8064f)]
        [DataRow(-3, -7.8f, -0.359f, -0.9333f)]
        public void Normalize(float x, float y, float expectedX, float expectedY)
        {
            Vector2 v = new Vector2(x, y);

            Vector2 normalizedV = Vector2.Normalize(v);
            Console.WriteLine("Normalized Vector: ");
            Console.WriteLine("\t\tX: " + Math.Round(normalizedV.Vx, 4));
            Console.WriteLine("\t\tY: " + Math.Round(normalizedV.Vy, 4));

            Assert.AreEqual(Math.Round(expectedX, 4), Math.Round(normalizedV.Vx, 4));
            Assert.AreEqual(Math.Round(expectedY, 4), Math.Round(normalizedV.Vy, 4));


            // same as Microsoft Vector2
            var mv1 = new Microsoft.Xna.Framework.Vector2(x, y);
            Microsoft.Xna.Framework.Vector2 normalizedMV = Microsoft.Xna.Framework.Vector2.Normalize(mv1);
            Assert.AreEqual(Math.Round(normalizedV.Vx, 4), Math.Round(normalizedMV.X), 4);
            Assert.AreEqual(Math.Round(normalizedV.Vy, 4), Math.Round(normalizedMV.Y), 4);
        }

        [DataTestMethod]
        [DataRow(5, 2, 0.9285f, 0.3714f)]
        [DataRow(6.6f, 9, 0.5914f, 0.8064f)]
        [DataRow(-3, -7.8f, -0.359f, -0.9333f)]
        public void DistanceSquared(float x1, float y1, float x2, float y2)
        {
            Vector2 v1 = new Vector2(x1, y1);
            Vector2 v2 = new Vector2(x2, y2);

            float distanceSquared = Vector2.DistanceSquared(v1, v2);

            // same as Microsoft Vector2
            var mv1 = new Microsoft.Xna.Framework.Vector2(x1, y1);
            var mv2 = new Microsoft.Xna.Framework.Vector2(x2, y2);
            float mDistanceSquared = Microsoft.Xna.Framework.Vector2.DistanceSquared(mv1, mv2);
            Assert.AreEqual(distanceSquared, mDistanceSquared);
        }
    }
}
