using Microsoft.VisualStudio.TestTools.UnitTesting;
using FlockingBackend;
using System.Collections.Generic;
using System;

namespace FlockingUnitTests
{
    [TestClass]
    public class FlockTest
    {
        static Flock Flock;
        static Raven Raven;
        static List<Sparrow> Sparrows;

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            Flock = new Flock();
            Raven = new Raven();
            Sparrows = new List<Sparrow>();

        }

        [TestMethod]
        public void CalculateBehaviourEventRaisingRandom()
        {
            List<Vector2> oldPositions = new List<Vector2>();
            for (int i = 0; i < 5; i++)
            {
                Sparrows.Add(new Sparrow());
                Flock.Subscribe(
                    Sparrows[i].CalculateBehaviour,
                    Sparrows[i].Move,
                    Sparrows[i].CalculateRavenAvoidance
                );
                oldPositions.Add(Sparrows[i].Position);
            }

            Flock.RaiseMoveEvents(Sparrows, Raven);

            for (int i = 0; i < Sparrows.Count; i++)
            {
                Assert.AreNotEqual(oldPositions[i], Sparrows[i].Position);
            }
        }

        [DataTestMethod]
        [DataRow(123f, 302f, 123, 300, 0, 2)]
        [DataRow(10f, 127f, 10, 123, 0, 6)]
        [DataRow(779.5858f, 601.5858f, 777, 599, 4, 4)]
        public void CalculateBehaviourEventRaising(float expectedX, float expectedY, float PosX, float PosY, float VelX, float VelY)
        {
            List<Sparrow> Sparrows = new List<Sparrow>();
            for (int i = 0; i < 3; i++)
            {
                Sparrows.Add(new Sparrow(PosX, PosY, VelX, VelY));
                Flock.Subscribe(
                    Sparrows[i].CalculateBehaviour,
                    Sparrows[i].Move,
                    Sparrows[i].CalculateRavenAvoidance
                );
            }

            Flock.RaiseMoveEvents(Sparrows, Raven);

            Assert.AreEqual(expectedX, Sparrows[0].Position.Vx);
            Assert.AreEqual(expectedY, Sparrows[0].Position.Vy);
        }
    }
}