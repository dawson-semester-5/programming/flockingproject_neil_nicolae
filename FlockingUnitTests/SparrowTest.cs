using Microsoft.VisualStudio.TestTools.UnitTesting;
using FlockingBackend;
using System.Collections.Generic;

namespace FlockingUnitTests
{
    [TestClass]
    public class SparrowTest
    {
        static Flock Flock;
        static Raven Raven;
        static List<Sparrow> Sparrows;

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            Flock = new Flock();
            Raven = new Raven();
            Sparrows = new List<Sparrow>();

            Sparrows.Add(new Sparrow(120, 548, -4, 3));
            Sparrows.Add(new Sparrow(121, 549, 1, 4));
            Sparrows.Add(new Sparrow(122, 550, -1, -3));
            Sparrows.Add(new Sparrow(123, 553, -1, 0));
            Sparrows.Add(new Sparrow(124, 552, -2, 3));
            Sparrows.Add(new Sparrow(125, 551, 2, 1));
        }

        [TestMethod]
        public void Alignment()
        {
            Vector2 v = Sparrows[0].Alignment(Sparrows);

            Assert.AreEqual(0.9789465f, v.Vx);
            Assert.AreEqual(0.20411679f, v.Vy);
        }

        [TestMethod]
        public void Cohesion()
        {
            Vector2 v = Sparrows[0].Cohesion(Sparrows);

            Assert.AreEqual(0.99968445f, v.Vx);
            Assert.AreEqual(-0.025118344f, v.Vy);
        }

        [TestMethod]
        public void Avoidance()
        {
            Vector2 v = Sparrows[0].Avoidance(Sparrows);

            Assert.AreEqual(0.19706827f, v.Vx);
            Assert.AreEqual(-0.9803898f, v.Vy);
        }
    }
}